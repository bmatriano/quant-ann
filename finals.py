import core
import csv
import numpy

average_error = sum_error =  [0,0,0]

# Change train.csv to the name of the file. Put file in same directory
with open('train.csv', 'r') as f:
    reader = csv.reader(f)
    data = list(reader)
    for x in range(0, len(data)):
        for y in range(0, 3):
            data[x][y] = float(data[x][y])

# Partition data here
split = 70
training = data[:split] # Gets the first 70 items of the set
validation = data[split:] # Gets everything else
# Change 70 to change tbe partitions of the the data
# Clear values to use all the data e.g. validation = data

# Arguments for ANN = topography, modes, weights (optional, and very hassle)
# Sample: core.ANN([3,1,3],['sigmoid','relu'])
# When setting output layer neuron numbers, make it as many as the possible values of the output
# e.g when classifying something which can be either 1,2 or 0, set output layer neurons to 3 e.g. [3,4,5,3]
# or, another example: classification can be either 0 or 1. set output layer neurons to 2, e.g. [2,2,1,2]
neural_network = core.ANN([4,2,3],['sigmoid','sigmoid'])


# Data notes
# Place classification in the first column

for x in range(0,len(training)):
    result = neural_network.feed_forward(neural_network.modes,training[x][1:],neural_network.weights)[-1]
    error = []
    classification = [0]*neural_network.topography[-1]
    classification[int(training[x][0])] = 1
    for x in range(0,len(result)):
        difference = classification[x]-result[x]
        error.append((difference)**2)
    for x in range(0,len(result)):
        sum_error[x] = sum_error[x] + error[x]


# Change the base values in the input csv to zero
# e.x. possible classifications of 2,3,4
#      change the csv to read 0,1,2
# OR
# e.x. possible classifications of 1,2,3,4,5
#      change it so the classifications in the csv read
#                                  0,1,2,3,4
for x in range(0,neural_network.topography[-1]):
    average_error[x] = sum_error[x]/len(training)

for x in range(0,len(validation)):
    classification = [0]*neural_network.topography[-1]
    classification[int(validation[x][0])] = 1
    result =  neural_network.back_propagation_batch(
        neural_network.modes,
        classification,
        validation[x][1:],
        neural_network.feed_forward(neural_network.modes,training[x][1:],neural_network.weights),
        neural_network.weights,
        average_error)
    neural_network.update_weights(result)

# If we need to feed-forward backprop feedforward-backprop sad :'(
# for x in range(0,5):
#   for x in range(0,len(validation)):
#      classification = [0]*neural_network.topography[-1]
#      classification[int(validation[x][0])] = 1
#      neural_network.train(40,training[x][1:],classification)
print(neural_network.network_settings())