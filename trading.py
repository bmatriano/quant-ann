import csv
import math
import matplotlib.pyplot as plt
import numpy


def aroon(data, time_length):
    """
    Returns the aroon up and aroon down values within a given series
    """
    highest = 0
    lowest = math.inf
    lowest_index = highest_index = 0
    for x in range(len(data) - 1, len(data) - time_length - 1, -1):
        if data[x] > highest:
            highest = data[x]
            highest_index = x
        if data[x] < lowest:
            lowest = data[x]
            lowest_index = x
    aroon_down = -(time_length - len(data) - 1 - lowest_index) / time_length
    aroon_up = -(time_length - len(data) - 1 - highest_index) / time_length
    return [aroon_up, aroon_down]


def RSI(data, time_length):
    loss_sum = 0
    gain_sum = 0
    for x in range(len(data) - 1, len(data) - time_length - 1, -1):
        if data[x] > data[x - 1]:
            gain_sum = gain_sum + data[x] - data[x - 1]
        elif data[x] < data[x - 1]:
            loss_sum = loss_sum + data[x - 1] - data[x]
    if (loss_sum == 0) & (gain_sum > 0):
        return 1
    relative_strength = gain_sum / loss_sum
    print(relative_strength)
    return (100 - 100 / (1 + relative_strength)) / 100


def ema(s, n):
    ema = []
    j = 1

    # get n sma first and calculate the next n period ema
    sma = sum(s[:n]) / n
    multiplier = 2 / float(1 + n)
    ema.append(sma)

    # EMA(current) = ( (Price(current) - EMA(prev) ) x Multiplier) + EMA(prev)
    ema.append(((s[n] - sma) * multiplier) + sma)

    # now calculate the rest of the values
    for i in s[n + 1:]:
        tmp = ((i - ema[j]) * multiplier) + ema[j]
        j = j + 1
        ema.append(tmp)
    return ema[-1]


def aroon_signal(data, time_length):
    signals = []
    for x in range(time_length, len(data)):
        # Checks if aroon up is greater than aroon down
        aroon_up = aroon(data[x - time_length:x], time_length)[0]
        aroon_down = aroon(data[x - time_length:x], time_length)[1]
        # Edit here to change trading thresholds for aroon up and down values
        if (aroon_up > .700) & (aroon_down <= .20):
            signals.append(1)
        elif (aroon_down > .700) & (aroon_up <= .20):
            signals.append(-1)
        else:
            signals.append(0)
    return signals


def macd_signal(data, short_ema, long_ema, signal_period):
    positions = []
    macd = []
    sn = []
    # If MACD is on top, 1, else, 0
    signals = []
    # 1, buy, -1, sell, 0 hold

    # [1,2,3,4,5,6,7,8,6,4,1,1,4,1,4,3,4,1,3,1,13,1,3,41,3] Input
    #                    [1,1,4,1,4,3,4,1,3,1,13,1,3,41,3] MACD
    #                            [1,2,3,4,1,1,1,1,1,1,1,2] Signal line

    for x in range(long_ema, len(data)):
        window = data[:x + 1]
        long_ema_value = ema(data[:x + 1], long_ema)
        short_ema_value = ema(data[:x + 1], short_ema)
        macd.append(short_ema_value - long_ema_value)

    for x in range(signal_period, len(macd)):
        sn.append(ema(macd[:signal_period + 1], signal_period))

    for x in range(0, len(sn)):
        if macd[x + signal_period] > sn[x]:
            positions.append(1)
        else:
            positions.append(0)

    # Indicator Positions to Trading Signals
    if positions[0] == 0:
        signals.append(-1)
    else:
        signals.append(1)
    for x in range(1, len(positions)):
        if positions[x] == positions[x - 1]:
            signals.append(0)
        else:
            if positions[x] == 1:
                signals.append(1)
            else:
                signals.append(-1)
    return signals


# [1,2,31,3,6,1,4,5,2,6,7,10,19,18,22,12]
def execute_trades(signals, data, start_balance=100000):
    success = fail = entry_price = holding = 0
    balance = start_balance
    history = []
    total = []
    history.append(balance)
    # Initial values
    if signals[0] == 1:
        holding = int(balance / data[len(data) - len(signals)])
        balance = balance - data[len(data) - len(signals)] * holding
        entry_price = data[len(data) - len(signals)]
        history.append(balance)
    else:
        holding = -(balance / data[len(data) - len(signals)])
        entry_price = data[len(data) - len(signals)]
        history.append(balance)

    # Main trading
    for x in range(len(data) - len(signals) + 1, len(data)):
        volume = (balance / data[x])
        # Buying/Short Exit
        sign = signals[x - (len(data) - len(signals))]
        if sign == 1:
            if holding < 0:
                if entry_price > data[x]:
                    success = success + 1
                elif entry_price < data[x]:
                    fail = fail + 1
                balance = balance - ((entry_price - data[x]) * holding)
                volume = (balance / data[x])
                balance = balance - (data[x] * volume)
                holding = volume
                entry_price = data[x]
                history.append(balance)
            elif holding == 0:
                volume = (balance / data[x])
                balance = balance - (data[x] * volume)
                holding = volume
                entry_price = data[x]
                history.append(balance)
        elif sign == -1:
            if holding > 0:
                if entry_price < data[x]:
                    success = success + 1
                elif entry_price > data[x]:
                    fail = fail + 1
                balance = balance + (data[x]) * holding
                volume = (balance / data[x])
                holding = -volume
                entry_price = data[x]
                history.append(balance)
            elif holding == 0:
                volume = (balance / data[x])
                holding = -volume
                entry_price = data[x]
                history.append(balance)
        else:
            history.append(balance)
        if holding > 0:
            total.append(balance + holding * entry_price)
        elif holding <= 0:
            total.append(balance)
    if holding > 0:
        balance = balance + data[-1] * holding
    else:
        balance = balance + (entry_price - data[-1]) * holding
    history.append(balance)
    output = [history, success, fail, total]
    return output


def optimize_aroon(data):
    max_profit = ideal = 0
    for x in range(5, len(data)):
        aroon_signals = aroon_signal(data, x)
        if execute_trades(aroon_signals, data)[0][-1] > max_profit:
            ideal = x
            max_profit = execute_trades(aroon_signals, data)[0][-1]
    return ideal


def optimal_aroon_display(data, begin, difference):
    signals = []
    # Algorithm divides into four, optimizes, then gets the optimal within the quarter of the data given
    for x in range(begin + 1, len(data)):
        aroon_up = aroon(data[x - begin:x], optimize_aroon(data[x - begin:x]))[0]
        aroon_down = aroon(data[x - begin:x], optimize_aroon(data[x - begin:x]))[1]
        add = 0
        if aroon_up > difference * aroon_down:
            if (len(signals) > 0):
                if (signals[-1] == 1):
                    add = 0
                else:
                    add = 1
        elif aroon_down > difference * aroon_up:
            if (len(signals) > 0):
                if (signals[-1] == -1):
                    add = 0
                else:
                    add = -1
        signals.append(add)
    total_history = execute_trades(signals, data)
    print('Final Profit', total_history[0][-1])
    print('Successes', total_history[1])
    print('Failures', total_history[2])
    plt.plot(total_history[3])
    plt.ylabel(total_history[3])
    plt.show()
    return total_history[3]


def optimal_aroon(data, begin, difference):
    signals = []
    # Algorithm divides into four, optimizes, then gets the optimal within the quarter of the data given
    for x in range(begin + 1, len(data)):
        aroon_up = aroon(data[x - begin:x], optimize_aroon(data[x - begin:x]))[0]
        aroon_down = aroon(data[x - begin:x], optimize_aroon(data[x - begin:x]))[1]
        add = 0
        if aroon_up > difference * aroon_down:
            if (x > 52) & (signals[-1] == 1):
                add = 0
            else:
                add = 1
        elif aroon_down > difference * aroon_up:
            if (x > 52) & (signals[-1] == -1):
                add = 0
            else:
                add = -1
        signals.append(add)
    total_history = execute_trades(signals, data)
    plt.plot(total_history[3])
    plt.show()
    return total_history[0][-1]


def sharpe_ratio(data):
    price_changes = []
    hprs = []
    car = 1
    for x in range(1, len(data)):
        if data[x] != data[x - 1]:
            price_changes.append(data[x])
    for x in range(1, len(price_changes)):
        hprs.append(price_changes[x] / price_changes[x - 1])
    for x in hprs:
        car = car * x
    car = car ** (1 / len(hprs))
    ahpr = numpy.mean(hprs)
    print(ahpr)
    print(numpy.std(hprs))
    print('Sharpe Ratio', (ahpr - 0.974) / numpy.std(hprs))
    print('CAR', car)


with open('DIS.csv', 'r') as f:
    reader = csv.reader(f)
    rows = list(reader)
    data = []
    for x in range(1, len(rows)):
        data.append(float(rows[x][0]))
with open('GOOG.csv', 'r') as f:
    reader = csv.reader(f)
    rows = list(reader)
    data2 = []
    for x in range(1, len(rows)):
        data2.append(float(rows[x][0]))
with open('INTC.csv', 'r') as f:
    reader = csv.reader(f)
    rows = list(reader)
    data3 = []
    for x in range(1, len(rows)):
        data3.append(float(rows[x][0]))
with open('GE.csv', 'r') as f:
    reader = csv.reader(f)
    rows = list(reader)
    data4 = []
    for x in range(1, len(rows)):
        data4.append(float(rows[x][0]))
with open('MSFT.csv', 'r') as f:
    reader = csv.reader(f)
    rows = list(reader)
    data5 = []
    for x in range(1, len(rows)):
        data5.append(float(rows[x][0]))




#
# def fin_net(data):
#     net = Core.ANN([1,3,1],['sigmoid','sigmoid'])
#     half_point = int(len(data)/2)
#     a_period = optimize_aroon(data[:half_point])
#     output = []
#     for x in range(a_period,half_point):
#         result = 1 if data[x+1]>data[x] else 0
#         aroon_up_value = aroon(data[x-a_period:x],a_period)[0]
#         relative_strength_index = RSI(data[:x],a_period)
#         net.train(10,[aroon_up_value],[result])
#     for x in range(half_point,len(data)-1):
#         aroon_up_value = aroon(data[x-a_period:x],a_period)[0]
#         relative_strength_index = RSI(data[:x],a_period)
#         result = 1 if data[x+1]>data[x] else 0
#         classification = net.feed_forward_display(net.modes,[aroon_up_value],net.weights)
#         output.append(classification[-1])
#         net.train(10,[aroon_up_value],[result])
#     print(output)
#     return output
#
#
#
#
#
# def optimize(data):
#     max_profit = p = t = 0
#     for x in range(2,100):
#         for y in range(0.5,3,0.125):
#             if optimal_aroon(data,x,y)>max_profit:
#                 p = x
#                 t = y
#     optimal_aroon_display(data,p,t)
#
# optimize(data)
# optimal_aroon(data2)
# #
# # x = []
# # for y in range(0,144):
# #     x.append(y)
# #
# # # x = [1,2,4,4,5,4,5,6,7,8,13,11,12,14,15,16,14,17,13,12,10,15,12,11,10,9,5,2,1,2,41,4,5,2,1,3,5,7,5,3,1,2,4,5,6,4,7,8,9,10,15,12,11,10,9,5,2,1,2,41,4,5,2,1,3,5,7,5,3,1,2,4,5,6,4,7,8,9,10,15,12,11,10,9,5,2,1,2,41,4,5,2,1,3,5,7,5,3,1,2,4,5,6,4,7,8,9,10,15,12,11,10,9,5,2,1,2,41,4,5,2,1,3,5,7,5,3,1,2,4,5,6,4,7,8,9,10,15,12,11,10,9,5,2]
# # fin_net(x)
