import core
import csv
import matplotlib.pyplot as plt


class execute(object):
    # Network Instantiation

    autoencoder_error = []
    classifier_error = []
    standalone_error = []

    def __init__(self):
        self.classifier = core.ANN([4,1,3,2,3],['relu','sigmoid','sigmoid','sigmoid'])
        self.autoencoder = core.ANN([5,4,4,4,5],['relu','sigmoid','sigmoid','relu'])
        self.standalone = core.ANN([5,2,3,4,3],['relu','sigmoid','sigmoid','sigmoid'])

    # Data Preparation
    with open('train.csv', 'r') as f:
        reader = csv.reader(f)
        data = list(reader)
        for x in range(0,len(data)):
            for y in range(0,6):
                data[x][y] = float(data[x][y])
    training = data[:70]
    validation = data[70:100]

    # Network Initializations
    def initialize_autoencoder(self, topography, modes, weights=None):
        if weights == None:
            self.autoencoder.weights = self.autoencoder.weights_init(topography)
            self.autoencoder.topography = topography
        else:
            self.autoencoder.weights = weights
        self.autoencoder.modes = modes
    def initialize_classifier(self, topography, modes, weights=None):
        if weights == None:
            self.classifier.weights = self.classifier.weights_init(topography)
            self.classifier.topography = topography
        else:
            self.classifier.weights = weights
        self.classifier.modes = modes
    def initialize_standalone(self, topography, modes, weights=None):
        if weights == None:
            self.standalone.weights = self.standalone.weights_init(topography)
            self.standalone.topography = topography
        else:
            self.standalone.weights = weights
        self.standalone.modes = modes

    # Hybrid System Training
    def train_hybrid(self):
        for x in range(0, 70):
            result = []
            if self.training[x][0] == 1:
                result = [1,0,0]
            elif self.training[x][0]== 2:
                result = [0,1,0]
            else:
                result = [0,0,1]
            self.autoencoder.train(20, self.training[x][1:],self.training[x][1:])
            self.autoencoder_error.append(self.autoencoder.error[-1])
            mid = int((len(self.autoencoder.topography) + 1) / 2)-2
            autoencoder_result = self.autoencoder.feed_forward(self.autoencoder.modes, self.training[x][1:], self.autoencoder.weights)[mid]
            print(autoencoder_result)
            self.classifier.train(20, autoencoder_result, result)
            self.classifier_error.append(self.classifier.error[-1])
    def train_standalone(self):
        for x in range(0,70):
            result = []
            if self.training[x][0] == 1:
                result = [1,0,0]
            elif self.training[x][0]== 2:
                result = [0,1,0]
            else:
                result = [0,0,1]
            self.standalone.train(20,self.training[x][1:],result)
            self.standalone_error.append(self.standalone.error[-1])

    #Utility
    def show_error_graphs(self):
        plt.figure(1)
        plt.plot(self.standalone_error)
        plt.xlabel('Standalone Classifier Error')
        plt.figure(2)
        plt.plot(self.autoencoder_error)
        plt.xlabel('Autoencoder Error ')
        plt.figure(3)
        plt.plot(self.classifier_error)
        plt.xlabel('Classifier Error')
        plt.show()
    def classify(self):
        successes_hybrid = 0
        successes_standalone = 0
        for x in range(0,30):
            raw_y_standalone = self.standalone.feed_forward(self.standalone.modes,self.validation[x][1:],self.standalone.weights)[-1]
            for y in range(1,3):
                print(raw_y_standalone)
                if raw_y_standalone[y]>raw_y_standalone[y-1]:
                    raw_y_standalone[y] = 1
                    raw_y_standalone[y-1] = 0
            if raw_y_standalone[int(self.validation[x][0])-1] == 1:
                successes_standalone =  successes_standalone + 1
            raw_y_hybrid = self.classifier.feed_forward(self.classifier.modes,
                                                        self.autoencoder.feed_forward(self.autoencoder.modes,self.validation[x][1:],self.autoencoder.weights)[0],
                                                        self.classifier.weights)[-1]
            for y in range(1,3):
                print(raw_y_hybrid)
                if raw_y_hybrid[y]>raw_y_hybrid[y-1]:
                    raw_y_hybrid[y] = 1
                    raw_y_hybrid[y-1] = 0
            if raw_y_hybrid[int(self.validation[x][0])-1] == 1:
                successes_hybrid = successes_hybrid+1
        return [successes_hybrid,successes_standalone]

    def ally_over(self):
        self.initialize_autoencoder(self.autoencoder.topography,self.autoencoder.modes)
        self.initialize_classifier(self.classifier.topography,self.classifier.modes)
        self.initialize_standalone(self.standalone.topography,self.standalone.modes)
        self.train_hybrid()
        self.train_standalone()
        self.show_error_graphs()
        x = self.classify()
        print(x[0],'Mixed autoencoder and classifier method successes')
        print(x[1],'Classifier method successes')

fin = execute();
fin.ally_over()